package xyz.gioeledevitti.dsa;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
// How many iterations to run for each trial/fork and how long each iteration should take
@Warmup(iterations = 3000, time = 15, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 1000, time = 15, timeUnit = TimeUnit.MILLISECONDS)
// Timeout for each iteration
@Timeout(time = 100, timeUnit = TimeUnit.MILLISECONDS)
// How many trials/forks to compute for each benchmark
@Fork(5)
public class BenchmarkPartitioning {

    @State(Scope.Thread)
    public static class Input {

        public int[] A;
        // Size of input array
        @Param({"1000", "10000", "100000", "1000000"})
        public int size;

        private final Random rnd = new Random();

        @Setup(Level.Iteration)
        public void prepareInput() {
            A = new int[size];
            for (int i = 0; i < size; i++)
                A[i] = rnd.nextInt();
        }

    }

    @Benchmark
    public static void partitionLomuto(Input in, Blackhole blackhole) {
        int l = 0, r = in.A.length - 1;
        int p = in.A[r];
        int ll = l - 1;

        for (int bu = l; bu < r; bu++) {
            if (in.A[bu] <= p) {
                ll++;
                swap(in.A, ll, bu);
            }
        }
        swap(in.A, ll + 1, r);

        blackhole.consume(in.A);
        blackhole.consume(ll);
    }

    @Benchmark
    public static void partitionHoare(Input in, Blackhole blackhole) {
        int p = in.A[in.A.length - 1];
        int l = -1, r = in.A.length;

        while (true) {
            do l++; while (in.A[l] < p);
            do r--; while (in.A[r] > p);
            if (l >= r)
                break;
            swap(in.A, l, r);
        }

        blackhole.consume(in.A);
        blackhole.consume(l);
    }

    @Benchmark
    public static void partitionOutsideIn(Input in, Blackhole blackhole) {
        int pivot = in.A[in.A.length - 1];
        int l = 0, r = in.A.length - 2;

        while (l < r) {
            while (in.A[l] < pivot)
                l++;
            while (r >= 0 && in.A[r] >= pivot)
                r--;
            if (l < r) {
                swap(in.A, l, r);
                l++;
                r--;
            }
        }

        blackhole.consume(in.A);
        blackhole.consume(l);
    }

    private static void swap(int[] A, int a, int b) {
        int tmp = A[a];
        A[a] = A[b];
        A[b] = tmp;
    }

}
