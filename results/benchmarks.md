# Benchmark results
Data collected throughout the research.

## Methodology

CPU utilization of other software has been minimized, not running any intensive applications in the background and
enabling offline mode.

## Definition of algorithms
* OutsideIn
```java
int partitionOutsideIn(int[] A) {
    int pivot = A[A.length - 1];
    int l = 0, r = A.length - 2;
        
    while (l < r) {
        while (A[l] < pivot)
            l++;
        while (r >= 0 && A[r] >= pivot)
            r--;
        if (l < r) {
            swap(A, l, r);
            l++;
            r--;
        }
    }
        
    return l;
}
```

* Lomuto
```java
int partitionLomuto(int[] A) {
    int l = 0, r = A.length - 1;
    int p = A[r];
    int ll = l - 1;

    for (int bu = l; bu < r; bu++) {
        if (A[bu] <= p) {
            ll++;
            swap(A, ll, bu);
        }
    }
    swap(A, ll + 1, r);
        
    return ll;
}
```

* Hoare
```java
int partitionHoare(int[] A) {
    int p = A[A.length - 1];
    int l = -1, r = A.length;

    while (true) {
        do l++; while (A[l] < p);
        do r--; while (A[r] > p);
        if (l >= r)
            break;
        swap(A, l, r);
    }

    return l;
}
```

## 1. Benchmarks run with the JMH library

### Instructions
1. Execute the maven goal `mvn clean package`.
2. Run the generated `benchmarks.jar` with `java -jar benchmarks.jar`.\
   *Note: Minimize hardware variation as much as possible by not running any resource-intensive application except the
    benchmark and not running the computer on battery power, as that may trigger energy-saving throttling.*

#### Meaning of data

* `size` = The size of the array that was fed to the algorithm.
* `Mode` = The modality of the benchmark, which in this case is to always measure the average running time (avgt).
* `Cnt` = How many times the algorithm has been executed. At each execution, the input array contains random integers
 ranging between all 2^32 possible values.  
* `Score` = How well the algorithm performed in terms of average running time (lower is better).
* `Error` = How much the min. and max. running time deviate from the average.
* `Units` = The unit used by `Score` and `Error`. `us/op` reads as microseconds per operation. One operation is
 equivalent to one execution of the algorithm.

### Benchmark run on an AMD Ryzen 5 3500U (OUTDATED)
* Forks: 5
* Warmup iterations: 3000, 15 ms
* Measurement iterations: 1000, 15 ms
```
Benchmark (size)  Mode   Cnt     Score    Error  Units
Hoare      1000  avgt  5000     0.240 ±  0.001  us/op
Hoare     10000  avgt  5000     2.385 ±  0.013  us/op
Hoare    100000  avgt  5000    24.696 ±  0.176  us/op
Hoare   1000000  avgt  5000   417.719 ±  4.770  us/op
Lomuto     1000  avgt  5000     1.635 ±  0.006  us/op
Lomuto    10000  avgt  5000    16.538 ±  0.079  us/op
Lomuto   100000  avgt  5000   168.021 ±  0.915  us/op
Lomuto  1000000  avgt  5000  2075.168 ± 10.922  us/op
```

### Benchmark run on an Intel Core I5 4690 (OUTDATED)
* Forks: 5
* Warmup iterations: 3000, 15 ms
* Measurement iterations: 1000, 15 ms
```
Benchmark (size)  Mode   Cnt     Score    Error  Units
Hoare      1000  avgt  5000     0.210 ±  0.001  us/op
Hoare     10000  avgt  5000     1.956 ±  0.003  us/op
Hoare    100000  avgt  5000    19.273 ±  0.020  us/op
Hoare   1000000  avgt  5000   376.459 ±  3.681  us/op
Lomuto     1000  avgt  5000     1.110 ±  0.001  us/op
Lomuto    10000  avgt  5000    10.999 ±  0.012  us/op
Lomuto   100000  avgt  5000   113.428 ±  0.220  us/op
Lomuto  1000000  avgt  5000  1498.317 ± 12.805  us/op
```

### Benchmark run on an AMD Ryzen 5 3500U (OUTDATED)
* Forks: 5
* Warmup iterations: 3000, 15 ms
* Measurement iterations: 1000, 15 ms
```
Benchmark          (size)  Mode   Cnt     Score    Error  Units
Hoare                1000  avgt  5000     0.242 ±  0.002  us/op
Hoare               10000  avgt  5000     2.380 ±  0.013  us/op
Hoare              100000  avgt  5000    24.480 ±  0.103  us/op
Hoare             1000000  avgt  5000   538.841 ±  6.381  us/op
HoareLecture         1000  avgt  5000     0.246 ±  0.002  us/op
HoareLecture        10000  avgt  5000     2.367 ±  0.012  us/op
HoareLecture       100000  avgt  5000    24.576 ±  0.129  us/op
HoareLecture      1000000  avgt  5000   560.350 ±  5.695  us/op
Lomuto               1000  avgt  5000     1.642 ±  0.007  us/op
Lomuto              10000  avgt  5000    16.532 ±  0.076  us/op
Lomuto             100000  avgt  5000   167.348 ±  0.760  us/op
Lomuto            1000000  avgt  5000  1993.435 ± 11.636  us/op
LomutoLecture        1000  avgt  5000     0.580 ±  0.008  us/op
LomutoLecture       10000  avgt  5000     6.827 ±  0.142  us/op
LomutoLecture      100000  avgt  5000    91.508 ±  1.858  us/op
LomutoLecture     1000000  avgt  5000  1158.609 ± 18.128  us/op
OutsideIn            1000  avgt  5000     0.207 ±  0.001  us/op
OutsideIn           10000  avgt  5000     2.739 ±  0.024  us/op
OutsideIn          100000  avgt  5000    27.074 ±  0.219  us/op
OutsideIn         1000000  avgt  5000   467.898 ±  6.864  us/op
```


## 2. Benchmarks run with start/end-time and sequential execution of algorithms

### Benchmark run on an AMD Ryzen 5 3500U

* input size = 100000

Ranking lists:
```
1) Hoare = 246.657234 us/op
2) Lomuto = 248.932707 us/op
3) OutsideIn = 261.608339 us/op
```

### Benchmark run on an AMD Ryzen 5 3500U

* input size = 1000000

Ranking lists:
```
1) Hoare = 2438.122376 us/op
2) OutsideIn = 2442.501900 us/op
3) Lomuto = 2578.189962 us/op
```

## 3. Benchmarks run measuring throughput of individual executions of the algorithms

### Benchmark run on an AMD Ryzen 5 3500U

* input size = 100,000

Ranking list (with 10 executions per different input array):
```
1) OutsideIn = 7139 ops/s = 140.075641 us/op
2) Hoare = 5987 ops/s = 167.028562 us/op
3) Lomuto = 3772 ops/s = 265.111347 us/op 
```
Ranking list (with 1 execution per different input array):
```
1) OutsideIn = 918 ops/s = 1089.324619 us/op
2) Hoare = 917 ops/s = 1090.512541 us/op
3) Lomuto = 900 ops/s = 1111.111111 us/op
```

* input size = 1,000,000

Ranking list (with 10 executions per different input array):
```
1) OutsideIn = 685 ops/s = 1459.854014 us/op
2) Hoare = 608 ops/s = 1644.736842 us/op
3) Lomuto = 393 ops/s = 2544.529262 us/op
```

## 4. Benchmarks run with start/end-time and sequential execution of algorithms, but with multiple executions per different input array

* input size = 100,000

Ranking list (with 10 executions per different input array):
```
1) Hoare: 26.282000 us/op
2) OutsideIn: 35.621000 us/op
3) Lomuto: 67.088000 us/op
```

Ranking list (with 1 execution per different input array):
```
1) Hoare: 287.923000 us/op
2) Lomuto: 288.821000 us/op
3) OutsideIn: 296.827000 us/op
```

Ranking list (with 1 execution per different input array and the -Xcomp flag enabled)
The -Xcomp flag forces compilation of methods on first invocation.
See [the docs](https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html) for further details.
```
1) Hoare: 294.578000 us/op
2) OutsideIn: 316.627000 us/op
3) Lomuto: 452.792000 us/op
```

## 5. Benchmarks run with start/end-time and sequential execution of algorithms, but without JIT compiler

* input size = 100,000

Ranking list (with 1 execution per different input array):
```
1) Hoare: 2329.131000 us/op
2) OutsideIn: 2822.450000 us/op
3) Lomuto: 4647.456000 us/op
```