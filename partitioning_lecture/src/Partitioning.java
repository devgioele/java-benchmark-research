import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;

public class Partitioning {

    public static int partitionLomuto(int[] A) {
        int l = 0, r = A.length - 1;
        int p = A[r];
        int ll = l - 1;

        for (int bu = l; bu < r; bu++) {
            if (A[bu] <= p) {
                ll++;
                swap(A, ll, bu);
            }
        }
        swap(A, ll + 1, r);

        return ll;
    }

    public static int partitionOutsideIn(int[] A) {
        int pivot = A[A.length - 1];
        int l = 0, r = A.length - 2;

        while (l < r) {
            while (A[l] < pivot)
                l++;
            while (r >= 0 && A[r] >= pivot)
                r--;
            if (l < r) {
                swap(A, l, r);
                l++;
                r--;
            }
        }

        return l;
    }

    public static int partitionHoare(int[] A) {
        int pivot = A[A.length - 1];
        int l = -1, r = A.length;

        while (true) {
            do l++; while (A[l] < pivot);
            do r--; while (A[r] > pivot);
            if (l >= r)
                break;
            swap(A, l, r);
        }

        return l;
    }

    public static void swap(int[] A, int i, int j) {
        int tmp;
        tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }

    public static int[] randomArray(int size) {
        Random random = new Random();
        int[] A = new int[size];
        for (int i = 0; i < size; i++) {
            A[i] = random.nextInt();
        }
        return A;
    }

    public static int[] arrayCopy(int[] A) {
        int[] B = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            B[i] = A[i];
        }
        return B;
    }

    public static double average(long[] A) {
        long sum = 0;

        for (long a : A)
            sum += a;

        return ((double) sum) / A.length;
    }

    public static double average(double[] A) {
        long sum = 0;

        for (double a : A)
            sum += a;

        return ((double) sum) / A.length;
    }

    public static float averageLong(long[] A) {
        long sum = 0;
        for (long a : A) {
            sum += a;
        }
        return ((float) sum) / A.length;
    }

    public static long numberOfSwapsLomuto(int[] A) {
        long swaps = 0;
        int pivot = A[A.length - 1];
        int ll = -1; // ll is the "last of the little numbers"
        for (int bu = 0; bu < A.length - 1; bu++) {
            // bu is the "beginning of the unknown"
            if (A[bu] < pivot) {
                ll++;
                swap(A, ll, bu);
                swaps++;
            }
        }
        return swaps;
    }

    public static long numberOfSwapsOutsideIn(int[] A) {
        long swaps = 0;

        int pivot = A[A.length - 1];
        int l = 0, r = A.length - 2;
        while (l < r) {
            while (A[l] < pivot) {
                l++;
            }
            ;
            while (r >= 0 && A[r] >= pivot) {
                r--;
            }
            ;
            if (l < r) {
                swap(A, l, r);
                swaps++;
                l++;
                r--;
            }
        }
        return swaps;
    }

    public static long numberOfSwapsHoare(int[] A) {
        int swaps = 0;

        int pivot = A[A.length - 1];
        int l = -1, r = A.length;
        while (true) {
            do {
                l++;
            } while (A[l] < pivot);
            do {
                r--;
            } while (A[r] > pivot);
            if (l < r) {
                swap(A, l, r);
                swaps++;
            } else {
                return swaps;
            }
        }
    }

    public static void averageRuntimeTests(Function<int[], Integer> algorithm, int size,
                                           int iterations, int repeatedExecs) {
        long start, end;
        int result;
        double[] runningTimes = new double[iterations];

        for (int i = 0; i < iterations; i++) {
            int[] A = randomArray(size);
            for (int j = 0; j < repeatedExecs; j++) {
                start = System.nanoTime();
                result = algorithm.apply(A);
                end = System.nanoTime();
                System.out.print(result);
                runningTimes[i] = (end - start) / 1000.0;
            }
        }
        System.out.printf("%nrunning time = %f%n", average(runningTimes));
    }

    public static void throughput(int size, int runMillis, int execsWithSameInput, int warmupExecs, int measureExecs) {
        int totalExecs = warmupExecs + measureExecs;
        long avg = 0;

        for (int i = 0; i < totalExecs; i++) {
            long nops = 0;
            long duration = 0L;
            long start = System.currentTimeMillis();
            while (duration < runMillis) {
                int[] A = randomArray(size);
                for (int j = 0; j < execsWithSameInput; j++) {
                    int result = partitionOutsideIn(A);
                    System.out.println(result);
                    nops++;
                }
                duration = System.currentTimeMillis() - start;
            }
            long throughput = 1000 * nops / duration;
            if (i >= warmupExecs)
                avg += throughput;
        }

        System.out.printf("~ %d ops/s%n", avg / measureExecs);
    }

    public static void swapTests(int size, int numberOfExecs) {
        double[] swapNumbersLomuto = new double[numberOfExecs + 1];
        double[] swapNumbersOutsideIn = new double[numberOfExecs + 1];
        double[] swapNumbersHoare = new double[numberOfExecs + 1];
        for (int i = 0; i <= numberOfExecs; i++) {
            int[] A = randomArray(size);
            int[] B = arrayCopy(A);
            int[] C = arrayCopy(A);
            swapNumbersLomuto[i] = numberOfSwapsLomuto(A);
            swapNumbersOutsideIn[i] = numberOfSwapsOutsideIn(B);
            swapNumbersHoare[i] = numberOfSwapsHoare(C);
        }
        System.out.println("Numbers of swaps for size " + size + ".");
        System.out.println("Lomuto: ");
        System.out.println(Arrays.toString(swapNumbersLomuto));
        System.out.println("Outside In: ");
        System.out.println(Arrays.toString(swapNumbersOutsideIn));
        System.out.println("Hoare: ");
        System.out.println(Arrays.toString(swapNumbersHoare));
    }

    public static void averageSwapsTests(int size, int range, int numberOfExecs) {
        long[] swapNumbersLomuto = new long[numberOfExecs + 1];
        long[] swapNumbersOutsideIn = new long[numberOfExecs + 1];
        long[] swapNumbersHoare = new long[numberOfExecs + 1];
        for (int i = 0; i <= numberOfExecs; i++) {
            int[] A = randomArray(size);
            int[] B = arrayCopy(A);
            int[] C = arrayCopy(A);
            swapNumbersLomuto[i] = numberOfSwapsLomuto(A);
            swapNumbersOutsideIn[i] = numberOfSwapsOutsideIn(B);
            swapNumbersHoare[i] = numberOfSwapsHoare(C);
        }
        System.out.println("Average numbers of swaps for size " + size + ".\n");
        System.out.println("Lomuto: ");
        System.out.println(averageLong(swapNumbersLomuto));
        System.out.println("Outside In: ");
        System.out.println(averageLong(swapNumbersOutsideIn));
        System.out.println("Hoare: ");
        System.out.println(averageLong(swapNumbersHoare));
    }

    private static int[][] prepareArrays(int amount, int size) {
        int[][] arrays = new int[amount][];

        for (int j = 0; j < arrays.length; j++)
            arrays[j] = randomArray(size);

        return arrays;
    }

    public static void main(String[] args) {
        int size = 100_00, iterations = 1000, repeatedExecs = 10;

        System.out.printf("input size = %d%n#iterations = %d%n#repeated executions = %d%n",
                size, iterations, repeatedExecs);

        System.out.println("---");
        System.out.println("Benchmarking Hoare");
        averageRuntimeTests(Partitioning::partitionHoare, size, iterations, repeatedExecs);

        System.out.println("---");
        System.out.println("Benchmarking Lomuto");
        averageRuntimeTests(Partitioning::partitionLomuto, size, iterations, repeatedExecs);
    }

    private static class RankItem implements Comparable<RankItem> {

        private final String name;
        private final Double score;

        public RankItem(String name, Double value) {
            this.name = name;
            this.score = value;
        }

        /**
         * @return a negative integer, zero, or a positive integer as this object
         * is less than, equal to, or greater than the specified object.\
         */
        @Override
        public int compareTo(RankItem o) {
            return score.compareTo(o.score);
        }

    }

}
